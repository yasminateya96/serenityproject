Feature: Medium contributors

  Scenario Outline: Get a list of contributors for a given publication
    When Add the "<Token>", "<Publication Id>" to the API then send the request
    Then Check the status code is <Status Code>
    Then Check the content type in the response header = "<Content Type>"
    Then Send the "<Token>", "<Publication Id>" to check the response contains the same "<Publication Id>"
    Then Send the "<Token>", "<Publication Id>" to check each one has a different user id
    Then Send the "<Token>", "<Publication Id>" to Check the returned role is editor

    Examples:
      | Token                                                                    | Publication Id | Status Code | Content Type                    |
      | Bearer 2c51b9d49d18005ebba3e4dbc7d7652dd6b86fdafad8b327b5bedf8259d4da891 | b45573563f5a   | 200         | application/json; charset=utf-8 |

  Scenario Outline: Check contributors with sending wrong token
    When Send the request with "<Publication Id>", wrong "<Token>"
    Then Check The status code is <Status Code>
    Then Check there is an error returned and it should has "<Message>" and <Error>
    Examples:
      | Token | Status Code | Message                      | Error |
      | x     | 401         | An access token is required. | 6000  |

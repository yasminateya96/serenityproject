Feature: Medium profile

  Scenario Outline: Get user information
    When Add the "<Token>" to the API then send the request
    Then The status code = <Status Code>
    Then The returned result should has "<User Name>", "<Name>" and "<URL>"
    Then Check the content type for the response header = "<Content Type>"

    Examples:
      | Token                                                                    | Status Code | User Name      | Name        | URL                                | Content Type                    |
      | Bearer 2c8ca846fc318d9b1e30f9dad651e76d29938ef4539255e9fafa01cdf6c07968b | 200         | yasminateya961 | Yasminateya | https://medium.com/@yasminateya961 | application/json; charset=utf-8 |

  Scenario Outline: Check user information with sending wrong token
    When Send the request with wrong "<Token>"
    Then Check The status code = <Status Code>
    Then There is error returned and it should has "<Message>" and <Error>
    Examples:
      | Token | Status Code | Message                      | Error |
      | x     | 401         | An access token is required. | 6000  |

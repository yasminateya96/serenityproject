package starter.apis;

public class AttributesOfMediumResponses {

    /*************** Profile feature attributes ***************/
    // +ve response (200)
    public static final String USER_NAME = "data.username";
    public static final String NAME = "data.name";
    public static final String URL = "data.url";

    /*************** Contributors feature attributes*****************/
    // +ve response (200)

    public static String getSpecific_PUBLICATION_ID(int i) {
        return "data[" + i + "].publicationId";
    }

    public static String getSpecific_USER_ID(int i) {
        return "data[" + i + "].userId";
    }

    public static String getSpecific_ROLE(int i) {
        return "data[" + i + "].role";
    }

    // -ve response (401)
    public static final String MESSAGE = "errors[0].message";
    public static final String CODE = "errors[0].code";
}



package starter.apis;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import static io.restassured.RestAssured.given;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class MediumContributorsStepDefinitions {

    @Steps
    MediumAPIsFactory mediumAPIsFactory;

    //+ve test cases
    @When("Add the {string}, {string} to the API then send the request")
    public void addTheTokenToThePublicationIdAPIThenSendTheRequest(String string, String string2) {
        MediumAPIsFactory.getContributors(string, string2);
    }

    @Then("Check the status code is {int}")
    public void checkStatusCodeIs200(int integer) {
        restAssuredThat(response -> response.statusCode(integer));
    }

    @Then("Check the content type in the response header = {string}")
    public void checkContentTypeOfResponseHeader(String string) {
        // Assert that the Content-Type = application/json; charset=utf-8
        restAssuredThat(response -> response.header("Content-Type", string));
    }

    @Then("Send the {string}, {string} to check the response contains the same {string}")
    public void sendTheTokenAndPublicationIdToCheckTheResponseContainsTheSamePublicationId(String string, String string2, String string3) {

        String contributorsResponse = MediumAPIsFactory.getContributors(string, string2)
                .then().extract().asString();

        JsonPath contributorsResponseJson = MediumAPIsFactory.rowToJson(contributorsResponse);
        int dataSize = contributorsResponseJson.getInt("data[0].size()");

        for (int i = 0; i < dataSize; i++) {

            Assert.assertEquals(contributorsResponseJson.get(AttributesOfMediumResponses.getSpecific_PUBLICATION_ID(i)), string3);
        }
    }

    @Then("Send the {string}, {string} to check each one has a different user id")
    public void sendTheTokenAndPublicationIdToCheckEachOneHasDifferentUserId(String string, String string2) {

        String contributorsResponse = MediumAPIsFactory.getContributors(string, string2)
                .then().extract().asString();

        JsonPath contributorsResponseJson = MediumAPIsFactory.rowToJson(contributorsResponse);
        int dataSize = contributorsResponseJson.getInt("data[0].size()");

        for (int i = 0; i < dataSize; i++) {

            String userId1 = contributorsResponseJson.get(AttributesOfMediumResponses.getSpecific_USER_ID(i));
            String userId2 = contributorsResponseJson.get(AttributesOfMediumResponses.getSpecific_USER_ID(i + 1));

            Assert.assertNotEquals(userId1, userId2);
        }
    }

    @Then("Send the {string}, {string} to Check the returned role is editor")
    public void sendTheTokenAndPublicationIdToCheckTheReturnedRoleIsEditor(String string, String string2) {

        String contributorsResponse = MediumAPIsFactory.getContributors(string, string2)
                .then().extract().asString();

        JsonPath contributorsResponseJson = MediumAPIsFactory.rowToJson(contributorsResponse);
        int dataSize = contributorsResponseJson.getInt("data[0].size()");

        for (int i = 0; i < dataSize; i++) {

            String role = contributorsResponseJson.get(AttributesOfMediumResponses.getSpecific_ROLE(i));
            Assert.assertEquals(role, "editor");
        }
    }


    //-ve test cases
    @When("Send the request with {string}, wrong {string}")
    public void sendTheRequestWithPublicationIdAndWrongToken(String string, String string2) {
        MediumAPIsFactory.getContributors(string, string2);
    }

    @Then("Check The status code is {int}")
    public void checkTheStatusCodeIs401(Integer integer) {
        restAssuredThat(response -> response.statusCode(integer));
    }

    @Then("Check there is an error returned and it should has {string} and {int}")
    public void checkThereIsAnErrorReturnedAndItShouldHasMessageAndError(String string, Integer int1) {
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.MESSAGE, equalTo(string)));
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.CODE, equalTo(int1)));
    }
}

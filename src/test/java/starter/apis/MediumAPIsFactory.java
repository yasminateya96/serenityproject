package starter.apis;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;


public class MediumAPIsFactory {

    private static String BASE_URI = "https://api.medium.com";

    private static String USER_INFO = "/v1/me";


    @Step("Get user information")
    public void getUserInfo(String token) {
        SerenityRest.given()
                .header("Authorization", token)
                .get(BASE_URI + USER_INFO);
    }


    @Step(" Get a list of contributors for a given publication id")
    public static Response getContributors(String token, String publicationId) {

        String CONTRIBUTORS_INFO = "/v1/publications/" + publicationId + "/contributors";

        return SerenityRest.given()
                .header("Authorization", token)
                .get(BASE_URI + CONTRIBUTORS_INFO);
    }

    @Step("Convert the string response to json and return it ")
    public static JsonPath rowToJson(String getApiResponseString) {
//        get in string and convert it to json and parsing it
        JsonPath getApiResponseJson = new JsonPath(getApiResponseString);
        return getApiResponseJson;
    }


}

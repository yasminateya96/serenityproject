package starter.apis;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class MediumProfileStepDefinitions {

    @Steps
    MediumAPIsFactory mediumAPIsFactory;

    //+ve test cases
    @When("Add the {string} to the API then send the request")
    public void addTheTokenToApiThenSendRequest(String string) {
        mediumAPIsFactory.getUserInfo(string);
    }

    @Then("The status code = {int}")
    public void statusCode200(int integer) {
        restAssuredThat(response -> response.statusCode(integer));
    }

    @Then("The returned result should has {string}, {string} and {string}")
    public void returnedResultShouldHas(String string, String string2, String string3) {

        restAssuredThat(response -> response.body(AttributesOfMediumResponses.USER_NAME, equalTo(string)));
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.NAME, equalTo(string2)));
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.URL, equalTo(string3)));

    }

    @Then("Check the content type for the response header = {string}")
    public void checkTheContentTypeForTheResponseHeader(String string) {
        restAssuredThat(response -> response.header("Content-Type", string));
    }

    //-ve test cases
    @When("Send the request with wrong {string}")
    public void sendTheRequestWithWrongToken(String string) {
        mediumAPIsFactory.getUserInfo(string);
    }

    @Then("Check The status code = {int}")
    public void checkTheStatusCode401(Integer integer) {
        restAssuredThat(response -> response.statusCode(integer));
    }

    @Then("There is error returned and it should has {string} and {int}")
    public void thereIsErrorReturnedShouldHas(String string, Integer code) {
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.MESSAGE, equalTo(string)));
        restAssuredThat(response -> response.body(AttributesOfMediumResponses.CODE, equalTo(code)));
    }

}

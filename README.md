# QA Back-end Assignment 
- This repo contains API testing automation for Medium endpoints using Serenity, RestAssured, Junit and Cucumber.
- I applied the code convention
- I committed my code with proper messages to represent my working flow (followed Udacity Git Commit Message Style Guide)

## 1. How to install
You can clone the project from here [Clone](https://gitlab.com/yasminateya96/serenityproject)

## 2. How to run
1. You can run the RunnerTestSuite class (src/test/java/starter/RunnerTestSuite.java) with the run button ▶️
2. You can run these commands from the terminal 
```bash
mvn clean test  
mvn clean verify  # generates a report
```
3. You can commit anything then the pipeline will run after pushing your commit

## 3. How to write test?
1. You can add another features to the (src/test/resources/features/medium)
2. Add class for the stepDefinitions to the (src/test/java/starter/apis)
3. Add the attributes of the response body to (AttributesOfMediumResponses.java)
4. Add your helpful methods to (MediumAPIsFactory.java)

## 4. Test reports
You can download the report as an artifact from the gitlab pipeline page

## 5. API documentation
I used the postman collection to add my documentation and some examples for the +ve and -ve scenarios so you will find (API docs) file which contains postman collection and the documentation
check the screenshots for more information
